import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;


public class Prikaz extends JFrame {
	
	 JPanel contentPane= new JPanel();


	/**
	 * Launch the application.
	 */
	public void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				while(true){
				try {
					Prikaz frame = new Prikaz();
					frame.setVisible(true);
					frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	public Prikaz() {
		
		setTitle("Prikaz covjeculjka");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 422, 451);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.revalidate();
	    contentPane.repaint();
		
		JLabel label_1 = new JLabel("");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setIcon(new ImageIcon("C:\\Lego\\Glava.png"));
		label_1.setBounds(80, 71, 200, 77);
		contentPane.add(label_1);
		label_1.revalidate();
	    label_1.repaint();
		

		
		JLabel label = new JLabel("");
		label.setBounds(80, 13, 191, 60);
		contentPane.add(label);
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon("C:\\Lego\\Kaciga.png"));
		
		JLabel label_2 = new JLabel("");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setIcon(new ImageIcon("C:\\Lego\\Tijelo.png"));
		label_2.setBounds(68, 144, 224, 120);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setIcon(new ImageIcon("C:\\Lego\\Noge.png"));
		label_3.setBounds(91, 261, 180, 130);
		contentPane.add(label_3);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				contentPane.revalidate();
			    contentPane.remove(0);
				contentPane.removeAll();
				setVisible(false);
				dispose();
				
			}
		});
		btnExit.setBounds(295, 366, 97, 25);
		contentPane.add(btnExit);
		contentPane.revalidate();
	    contentPane.repaint();
	}
}
