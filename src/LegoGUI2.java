import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Frame;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Choice;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.swing.ImageIcon;

import java.sql.*;

import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LegoGUI2 extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int idTijelo = 1;
	int pomoc=0;
	String text="";
	

	
	private JPanel contentPane;
	private JTextField textField;

	/**�
	 * Launch the application.
	 * @throws Exception 
	 */
	

	public static void main(String[] args) throws Exception {
		//Slike_glava object= new Slike_glava();
		Slike_glava.main(null);
		Slike_tijela.main(null);
		Slike_noge.main(null);
		Slike_kacige.main(null);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LegoGUI2 frame = new LegoGUI2();
					frame.setVisible(true);
					frame.repaint();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LegoGUI2() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 763, 542);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIzaberiSet = new JLabel("Izaberi set");
		lblIzaberiSet.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblIzaberiSet.setBounds(22, 13, 96, 41);
		contentPane.add(lblIzaberiSet);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Sve"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(130, 13, 142, 41);
		contentPane.add(comboBox);
		
		JButton btnZatvoriMe = new JButton("Zatvori me :(");
		btnZatvoriMe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
			}
		});
		
		
		btnZatvoriMe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnZatvoriMe.setBounds(512, 435, 131, 41);
		contentPane.add(btnZatvoriMe);
		
		JButton btnPrethodni = new JButton("Prethodni");
		btnPrethodni.setBounds(44, 122, 97, 25);
		contentPane.add(btnPrethodni);
		
		JButton button = new JButton("Prethodni");
		/*  
		  button.addMouseListener(new MouseAdapter() {
		 
			@Override
			public void mouseReleased(MouseEvent arg0) {
				
				test1 objekt = new test1();

				try {
					
					
					test1.proba(idTijelo);			
					
					JLabel lblNewLabel = new JLabel("");
				//	lblNewLabel.setDisplayedMnemonicIndex(-1);
				//	lblNewLabel.setDoubleBuffered(true);
					//lblNewLabel.setBorder(null);
				//	lblNewLabel.setRequestFocusEnabled(true);
					lblNewLabel.setVisible(true);
					//while(true){		
					
					//ImageIcon img = new ImageIcon("C:\\Users\\Nina\\Desktop\\nesto.png");
					BufferedImage img = ImageIO.read(new File("C:\\Users\\Nina\\Desktop\\nesto.png"));
					BufferedImage buffered = (BufferedImage) img;
					buffered.getGraphics().drawImage(img, 0, 0 , null);
					lblNewLabel.setIcon(new ImageIcon(img));
					img=null;
					lblNewLabel.setBounds(186, 181, 200, 114);	
					lblNewLabel.setVisible(true);
					contentPane.add(lblNewLabel);
					
					
				//	lblNewLabel.repaint();
					contentPane.revalidate();
					contentPane.repaint();
					//setVisible(true);
					toFront();
					repaint();
					
					//}
									
					
				
				} catch (Exception e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				idTijelo++;
			
		
				
				Tijelo newWindow = new Tijelo();
				newWindow.setVisible(true);
			}
		});
	
		*/		
		button.setBounds(44, 193, 97, 25);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Prethodni");
		button_1.setBounds(44, 270, 97, 25);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Prethodni");
		button_2.setBounds(44, 351, 97, 25);
		contentPane.add(button_2);
		
		JButton btnSlijedei = new JButton("Izaberi glavu");
		btnSlijedei.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browserGlava newWindow = new browserGlava();
				newWindow.setVisible(true);
				newWindow.setBounds(100, 100, 300, 200);
			}
		});
		btnSlijedei.setBounds(484, 166, 136, 25);
		contentPane.add(btnSlijedei);
		
		JButton btnIzaberiTijelo = new JButton("Izaberi tijelo");
		btnIzaberiTijelo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browser newWindow = new browser();
				newWindow.setVisible(true);
				newWindow.setBounds(100, 100, 300, 200);
			}
		});


		btnIzaberiTijelo.setBounds(484, 258, 136, 25);
		contentPane.add(btnIzaberiTijelo);
		
		JButton btnIzaberiNoge = new JButton("Izaberi noge");
		btnIzaberiNoge.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browserNoge newWindow = new browserNoge();
				newWindow.setVisible(true);
				newWindow.setBounds(100, 100, 300, 200);
				
			}
		});
		btnIzaberiNoge.setBounds(484, 351, 136, 25);
		contentPane.add(btnIzaberiNoge);
		
		JButton btnIzaberiDodatak = new JButton("Izaberi dodatak");
		btnIzaberiDodatak.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				browserDodatci newWindow = new browserDodatci();
				newWindow.setVisible(true);
				newWindow.setBounds(100, 100, 300, 200);
			
			}
		});
		btnIzaberiDodatak.setBounds(484, 84, 136, 25);
		contentPane.add(btnIzaberiDodatak);
		//while(true){		
		ImageIcon img = new ImageIcon("C:\\Users\\Nina\\Desktop\\nesto.png");
		//}
		
		JButton btnSpremi = new JButton("Spremi");
		btnSpremi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				 
				try {
					PrintWriter out = new PrintWriter("C:\\Lego\\ime.txt");
			        out.println(text);
			        out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		      
		    try {
				Upisi_Covjeculjka.main(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		       
			}
		});
		btnSpremi.setBounds(22, 432, 200, 50);
		contentPane.add(btnSpremi);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 text = textField.getText();
				 
			}
		});
		textField.setBounds(130, 67, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblUnesiIme = new JLabel("Unesi ime");
		lblUnesiIme.setBounds(12, 70, 56, 16);
		contentPane.add(lblUnesiIme);
	/*			PRIKAZ ODABRANOG TIJELA
		JLabel lblNewLabel = new JLabel("");
		
		lblNewLabel.setIcon(new ImageIcon(img.getImage()));
		lblNewLabel.setBounds(186, 181, 200, 114);
		contentPane.add(lblNewLabel);
		lblNewLabel.revalidate();
		lblNewLabel.repaint();
		*/
	}
}
