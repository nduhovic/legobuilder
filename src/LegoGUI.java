import java.awt.BorderLayout;
import java.awt.Window;
import java.io.InputStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;



public class LegoGUI {

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Shell shlLegoCovjeculjak = new Shell();
		shlLegoCovjeculjak.setImage(SWTResourceManager.getImage(LegoGUI.class, "/javax/swing/plaf/basic/icons/JavaCup16.png"));
		shlLegoCovjeculjak.setSize(631, 501);
		shlLegoCovjeculjak.setText("LEGO covjeculjak :)");
		
		Button btnIzradiCovjeculjka = new Button(shlLegoCovjeculjak, SWT.NONE);
		btnIzradiCovjeculjka.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnIzradiCovjeculjka.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				LegoGUI2 newWindow = new LegoGUI2();
				newWindow.setVisible(true);
				
			}
		});
		btnIzradiCovjeculjka.setBounds(51, 50, 201, 51);
		btnIzradiCovjeculjka.setText("Izradi covjeculjka");
		
		Button btnGalerijaCovjeculjaka = new Button(shlLegoCovjeculjak, SWT.NONE);
		btnGalerijaCovjeculjaka.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				
				try {
					Galerija.main(null);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnGalerijaCovjeculjaka.setText("Galerija covjeculjaka");
		btnGalerijaCovjeculjaka.setBounds(51, 130, 201, 51);
		
		Button btnPogledajDjelove = new Button(shlLegoCovjeculjak, SWT.NONE);
		btnPogledajDjelove.setText("Dodaj djelove");
		btnPogledajDjelove.setBounds(51, 212, 201, 51);
		
		Label label = new Label(shlLegoCovjeculjak, SWT.NONE);
		label.setAlignment(SWT.CENTER);
		label.setImage(SWTResourceManager.getImage(LegoGUI.class, "/Slike/LEGO.png"));
		label.setBounds(271, 12, 342, 388);
		
		Button btnZatvoriMe = new Button(shlLegoCovjeculjak, SWT.NONE);
		btnZatvoriMe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				System.exit(0);
			}
		});
		btnZatvoriMe.setBounds(487, 406, 115, 42);
		btnZatvoriMe.setText("Zatvori me  :(");

		shlLegoCovjeculjak.open();
		shlLegoCovjeculjak.layout();
		while (!shlLegoCovjeculjak.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
