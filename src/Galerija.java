import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;

public class Galerija extends JFrame {
	
	private JPanel contentPane;
	static ArrayList<String> niz = new ArrayList<String>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb","root","");
        String query = "SELECT Ime FROM `covjeculjak` ";
        Statement st = con.createStatement();
        ResultSet result2 = st.executeQuery(query);
        while (result2.next()){
        	niz.add(result2.getString(1));
        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Galerija frame = new Galerija();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Galerija() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 464, 206);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		for (int i=0;i<niz.size();i++){
			comboBox.addItem(niz.get(i));
		}
		comboBox.setBounds(168, 13, 155, 22);
		contentPane.add(comboBox);
		
		JLabel lblIzaberiCovjeculjka = new JLabel("Izaberi covjeculjka:");
		lblIzaberiCovjeculjka.setBounds(12, 16, 144, 16);
		contentPane.add(lblIzaberiCovjeculjka);
		
		JButton btnPrikazi = new JButton("Prikazi");
		btnPrikazi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				try {
					String x = (String)comboBox.getSelectedItem();
					PrintWriter out = new PrintWriter("C:\\Lego\\IME.txt");
					out.println(x);
					out.close();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					Covjek_iz_baze.main(null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					Thread.sleep(2000);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Prikaz t = new Prikaz();
				t.main(null);
				
			/*	contentPane.revalidate();
				contentPane.validate();
			    contentPane.remove(0);
				contentPane.removeAll();
				*/
	//			WindowEvent closingEvent = new WindowEvent(newWindow, WindowEvent.WINDOW_CLOSING);
		//	    Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closingEvent);
			      
				
			}
		});
		btnPrikazi.setBounds(95, 76, 97, 25);
		contentPane.add(btnPrikazi);
		
		JButton btnZatvori = new JButton("Zatvori");
		btnZatvori.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				niz.removeAll(niz);
				dispose();
			}
		});
		btnZatvori.setBounds(239, 76, 97, 25);
		contentPane.add(btnZatvori);
		
		
	}
}
