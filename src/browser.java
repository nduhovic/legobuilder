import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.TimerTask;


public class browser extends JFrame
{
    //private ImageIcon myImage1 = new ImageIcon ("C:\\Users\\Nina\\Desktop\\nesto1.png");
    //private ImageIcon myImage2 = new ImageIcon ("C:\\Users\\Nina\\Desktop\\nesto2.png");
    //private ImageIcon myImage3 = new ImageIcon ("C:\\Users\\Nina\\Desktop\\nesto3.png");
    //private ImageIcon myImage4 = new ImageIcon ("C:\\Users\\Nina\\Desktop\\nesto4.png");
    private int i=0;
    private int pozicija=0;
    private int max=0;
    JPanel ImageGallery = new JPanel();
    private ImageIcon[] myImages = new ImageIcon[4];
    private int curImageIndex=0;

    public browser ()
        {   
    
    		Scanner scanner;
			try {
				scanner = new Scanner (new File("C:\\Lego\\Tijela\\Broj_tijela.txt"));
				max=scanner.nextInt();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ImageGallery.add(new JLabel (myImages[1]));
            int pomoc=0;
            for (i=1;i<max+1;i++){
            	myImages[pomoc]=new ImageIcon ("C:\\Lego\\Tijela\\tijelo"+i+".png"); ;
            	pomoc++;
            }
            add(ImageGallery, BorderLayout.CENTER);

            JButton PREVIOUS = new JButton ("Previous");
            JButton PLAY = new JButton ("Exit");
            JButton STOP = new JButton ("Save");
            JButton NEXT = new JButton ("Next");

            JPanel Menu = new JPanel();
            Menu.setLayout(new GridLayout(1,4));
            Menu.add(PREVIOUS);
            Menu.add(PLAY);
            Menu.add(STOP);
            Menu.add(NEXT);

            add(Menu, BorderLayout.SOUTH);

            //register listener
            PreviousButtonListener PreviousButton = new PreviousButtonListener ();
            PlayButtonListener PlayButton = new PlayButtonListener ();
            StopButtonListener StopButton = new StopButtonListener ();
            NextButtonListener NextButton = new NextButtonListener ();

            //add listeners to corresponding componenets 
            PREVIOUS.addActionListener(PreviousButton);
            PLAY.addActionListener(PlayButton);
            STOP.addActionListener(StopButton);
            NEXT.addActionListener(NextButton);

        }

    public static void main (String [] args)
        {
            browser frame = new browser();

            frame.setSize(490,430);
            frame.setVisible(true);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setLocationRelativeTo(null);
        }



    class PreviousButtonListener implements ActionListener 
    {

        public void actionPerformed(ActionEvent e)
            {
        	if (pozicija!=0)
        		pozicija--;
                if(curImageIndex>0 && curImageIndex <= max-1)
                    {   ImageGallery.remove(0);
                        curImageIndex=curImageIndex-1;
                        ImageIcon TheImage= myImages[curImageIndex];
                        ImageGallery.add(new JLabel (TheImage));
                        ImageGallery.validate();
                        ImageGallery.repaint(); 
                    }
                else 
                    {   
                        ImageGallery.remove(0);
                        ImageGallery.add(new JLabel (myImages[0]));
                        curImageIndex=0;
                        ImageGallery.validate();
                        ImageGallery.repaint();
                    }
            }
    }

    class PlayButtonListener implements ActionListener 
    {
        public void actionPerformed(ActionEvent e)
            {
        	dispose();

            }
    }

    class StopButtonListener implements ActionListener 
    {
        public void actionPerformed(ActionEvent e)
            {
        	PrintWriter out;
			try {
				out = new PrintWriter("C:\\Lego\\tijelo.txt");
		       	out.println(pozicija+1);
	        	out.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 
            }
    }

    class NextButtonListener implements ActionListener 
    {

        public void actionPerformed(ActionEvent e)
        {
        	if (pozicija+1<max)
        		pozicija++;
            if(curImageIndex>=0 && curImageIndex < max-1)
                {   ImageGallery.remove(0);
                    curImageIndex = curImageIndex + 1;
                    ImageIcon TheImage= myImages[curImageIndex];
                    ImageGallery.add(new JLabel (TheImage));
                    ImageGallery.validate();
                    ImageGallery.repaint(); 
                }
            else 
                {   
                    ImageGallery.remove(0);
                    ImageGallery.add(new JLabel (myImages[max-1]));
                    curImageIndex=max-1;
                    ImageGallery.validate();
                    ImageGallery.repaint();
                }

        }
    }
}